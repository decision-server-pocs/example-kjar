package pocs.puckboard;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.drools.core.event.DefaultAgendaEventListener;
import org.junit.Ignore;
import org.junit.Test;
import org.kie.api.KieServices;
import org.kie.api.command.BatchExecutionCommand;
import org.kie.api.command.Command;
import org.kie.api.command.KieCommands;
import org.kie.api.event.rule.AfterMatchFiredEvent;
import org.kie.api.runtime.ExecutionResults;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.StatelessKieSession;
import org.kie.internal.command.CommandFactory;
import org.kie.server.api.marshalling.MarshallingFormat;
import org.kie.server.api.model.KieServiceResponse.ResponseType;
import org.kie.server.api.model.ServiceResponse;
import org.kie.server.client.KieServicesClient;
import org.kie.server.client.KieServicesConfiguration;
import org.kie.server.client.KieServicesFactory;
import org.kie.server.client.RuleServicesClient;

public class RuleTest {

	private boolean debug = true;

	@Ignore
	@Test
	public void testThis() {

		KieContainer kieContainer = KieServices.Factory.get().getKieClasspathContainer();

		List<Command<?>> commands = new ArrayList<Command<?>>();
		commands.add(CommandFactory.newEnableAuditLog("auditFile.txt"));
		commands.add(CommandFactory.newFireAllRules());

		StatelessKieSession ksession = kieContainer.newStatelessKieSession();
		if (debug) {
			ksession.addEventListener(new DefaultAgendaEventListener() {
				public void afterMatchFired(AfterMatchFiredEvent event) {
					super.afterMatchFired(event);
					System.out.println(event);
				}
			});
		}
		BatchExecutionCommand batch = CommandFactory.newBatchExecution(commands);
		ExecutionResults results = ksession.execute(batch);

	}

	@Ignore
	@Test
	public void testThisAlso() {
		
		//String url = "https://secure-myapp-kieserver-puckboard-playground.apps.s11.core.rht-labs.com/kie-server/services/rest/server";
		//String url = "https://secure-myapp-kieserver-puckboard-playground.apps.s11.core.rht-labs.com";
		String url = "https://secure-myapp-kieserver-puckboard-playground.apps.s11.core.rht-labs.com/services/rest/server";
		String username = "adminUser";
		String password = "nMwNFA0!";
		String container = "c1";
		String session = "defaultStatelessKieSession";

		KieServicesConfiguration conf = KieServicesFactory.newRestConfiguration(url, username, password);
		conf.setMarshallingFormat(MarshallingFormat.JSON);

		KieServicesClient kieServicesClient = KieServicesFactory.newKieServicesClient(conf);

		System.out.println("== Sending commands to the server ==");
		RuleServicesClient rulesClient = kieServicesClient.getServicesClient(RuleServicesClient.class);
		KieCommands commandsFactory = KieServices.Factory.get().getCommands();

		//Command<?> insert = commandsFactory.newInsert("Some String OBJ");
		Command<?> fireAllRules = commandsFactory.newFireAllRules();
		Command<?> batchCommand = commandsFactory.newBatchExecution(Arrays.asList(fireAllRules), session);

		ServiceResponse<String> executeResponse = rulesClient.executeCommands(container, batchCommand);

		if (executeResponse.getType() == ResponseType.SUCCESS) {
			System.out.println("Commands executed with success! Response: ");
			System.out.println(executeResponse.getResult());
		} else {
			System.out.println("Error executing rules. Message: ");
			System.out.println(executeResponse.getMsg());
		}

	}

}
